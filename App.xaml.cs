﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Prozessteuerung
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static ObservableCollection<Process> _processList;

        private void Application_Startup(object sender, StartupEventArgs e)
        {            
            _processList = StorageData.readXML<ObservableCollection<Process>>("AppData.xml");

            if (_processList == null)
            {
                _processList = new ObservableCollection<Process>();
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            StorageData.writeXML<ObservableCollection<Process>>(_processList, "AppData.xml");
        }
    }
}
