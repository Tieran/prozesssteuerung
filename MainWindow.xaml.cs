﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Prozessteuerung
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random rnd = new Random();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lbx_Process.ItemsSource = App._processList;
        }

        private void b_testData_Click(object sender, RoutedEventArgs e)
        {
            Test_Methods testData = new Test_Methods();
            App._processList.Add(testData.generateProcess());
        }

        private void lbx_Process_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbx_Process.SelectedItem != null)
            {
                EditWindow edWin = new EditWindow();
                edWin.proc = lbx_Process.SelectedItem as Process;
                edWin.Show();
            }

        }

        private void b_addNew_Click(object sender, RoutedEventArgs e)
        {
            EditWindow edWin = new EditWindow();
            edWin.proc = new Process();
            edWin.Show();
        }

        private void b_delSelec_Click(object sender, RoutedEventArgs e)
        {
            if (lbx_Process.SelectedItem != null)
            {
                App._processList.Remove(lbx_Process.SelectedItem as Process);
            }
        }
    }
}
