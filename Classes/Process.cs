﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prozessteuerung
{
    public class Process
    {
        public Employee employee { get; set; }
        public Task task { get; set; }

        public Process()
        {
            employee = new Employee();
            task = new Task();
        }
    }
}
