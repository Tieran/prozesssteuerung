﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prozessteuerung
{ 
    public class Task
    {
        public string name { get; set; }
        public string description { get; set; }
        public DateTime startTime { get; set; }
        public DateTime finishTime { get; set; }
    }
}
