﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prozessteuerung
{
    public class Test_Methods
    {
        Random rnd = new Random();

        private string[] female_forenameArray = { "Agnes", "Agnieszka", "Alexandra", "Alina", "Alma", "Amelie", "Andrea", "Anett", "Anette", "Angela", "Angelica", "Angelika", "Anica", "Anika", "Anita", "Anja", "Anke", "Ann", "Anna", "Anne", "Anneliese", "Annemarie", "Annett", "Annette", "Anni", "Annica", "Annie", "Annika", "Anny", "Antje", "Antonia", "Ariane", "Astrid", "Auguste", "Ayse", "Bärbel", "Barbara", "Beata", "Beate", "Beatrice", "Berit", "Berta", "Bertha", "Bettina", "Bianca", "Bianka", "Birgit", "Birgitt", "Birte", "Birthe", "Brigitte", "Britta", "Caren", "Carina", "Carla", "Carmen", "Carola", "Carolin", "Caroline", "Catarina", "Catharina", "Cathleen", "Cathrin", "Catrin", "Celina", "Charlotte", "Christa", "Christel", "Christiane", "Christin", "Christina", "Christine", "Cindy", "Clara", "Claudia", "Constanze", "Cordula", "Corinna", "Cornelia", "Dörte", "Dörthe", "Dagmar", "Dana", "Daniela", "Denise", "Diana", "Dora", "Doreen", "Doris", "Dorothea", "Dorothee", "Edith", "Elena", "Elfriede", "Elisabeth", "Elise", "Elke", "Ella", "Elli", "Elly", "Elsa", "Else", "Emilia", "Emilie", "Emily", "Emine", "Emma", "Erika", "Erna", "Esther", "Eva", "Evelin", "Eveline", "Evelyn", "Evelyne", "Ewa", "Fatma", "Filiz", "Franziska", "Frauke", "Frida", "Frieda", "Friederike", "Gabi", "Gabriela", "Gabriele", "Gaby", "Gerda", "Gertrud", "Gesa", "Gisela", "Grit", "Hanna", "Hannah", "Hannelore", "Hatice", "Hedwig", "Heidi", "Heike", "Helena", "Helene", "Helga", "Henni", "Henny", "Herta", "Hertha", "Hildegard", "Ida", "Ilka", "Ilona", "Ilse", "Imke", "Ina", "Ines", "Inga", "Inge", "Ingeborg", "Ingrid", "Irene", "Irina", "Iris", "Irma", "Irmgard", "Isabel", "Isabell", "Isabella", "Isabelle", "Ivonne", "Jacqueline", "Jana", "Janet", "Janett", "Janette", "Janin", "Janina", "Janine", "Jaqueline", "Jasmin", "Jeanette", "Jeannette", "Jennifer", "Jenny", "Jessica", "Jessika", "Joanna", "Johanna", "Judith", "Julia", "Juliane", "Jutta", "Käte", "Käthe", "Karen", "Karin", "Karina", "Karla", "Karola", "Karolin", "Karoline", "Katarina", "Katarzyna", "Katharina", "Kathi", "Kathie", "Kathleen", "Kathrin", "Kati", "Katie", "Katja", "Katrin", "Kerstin", "Kim", "Kirsten", "Kirstin", "Klara", "Klaudia", "Konstanze", "Kordula", "Korinna", "Kornelia", "Kristiane", "Kristin", "Kristina", "Kristine", "Lara", "Larissa", "Laura", "Lea", "Leah", "Lena", "Leni", "Leoni", "Leonie", "Lidia", "Lieselotte", "Lili", "Lilli", "Lilly", "Lina", "Linda", "Lisa", "Liselotte", "Lotte", "Louisa", "Louise", "Luisa", "Luise", "Lydia", "Magdalena", "Maike", "Maja", "Malgorzata", "Mandy", "Manja", "Manuela", "Mareike", "Maren", "Margarete", "Margarethe", "Margot", "Margrit", "Maria", "Marianne", "Marie", "Marina", "Marion", "Marta", "Martha", "Martina", "Maya", "Meike", "Melanie", "Melina", "Melissa", "Meta", "Metha", "Mia", "Michaela", "Michelle", "Minna", "Miriam", "Mirja", "Mirjam", "Monika", "Monique", "Nadin", "Nadine", "Nadja", "Nancy", "Natalia", "Natalie", "Natascha", "Nathalie", "Neele", "Nele", "Nicola", "Nicole", "Nikola", "Nina", "Olga", "Pamela", "Patricia", "Patrizia", "Paula", "Peggy", "Petra", "Ramona", "Rebecca", "Rebekka", "Regina", "Renate", "Rita", "Rosemarie", "Ruth", "Sabine", "Sabrina", "Sandra", "Sara", "Sarah", "Saskia", "Sibylle", "Sigrid", "Silke", "Silvia", "Simone", "Sina", "Sinah", "Sofia", "Sofie", "Sonja", "Sophia", "Sophie", "Stefanie", "Steffi", "Stephanie", "Susan", "Susann", "Susanne", "Svantje", "Svenja", "Svetlana", "Swantje", "Swenja", "Swetlana", "Sybille", "Sylke", "Sylvia", "Sylwia", "Tamara", "Tania", "Tanja", "Tatjana", "Tina", "Ulrike", "Ursula", "Uta", "Ute", "Vanessa", "Vera", "Verena", "Veronica", "Veronika", "Victoria", "Viktoria", "Viola", "Waltraud", "Waltraut", "Wera", "Wibke", "Wiebke", "Wilhelmine", "Yasemin", "Yasmin", "Yvonne" };

        private string[] male_forenameArray = { "Adolf", "Albert", "Alexander", "Alfred", "André", "Andre", "Andreas", "Arthur", "Artur", "August", "Axel", "Ben", "Benjamin", "Bernd", "Björn", "Bruno", "Carl", "Carsten", "Christian", "Christoph", "Claus", "Curt", "Daniel", "David", "Dennis", "Dieter", "Dirk", "Dominic", "Dominik", "Elias", "Emil", "Eric", "Erich", "Erik", "Ernst", "Erwin", "Fabian", "Felix", "Finn", "Florian", "Frank", "Franz", "Friedrich", "Fritz", "Fynn", "Günter", "Günther", "Georg", "Gerd", "Gerhard", "Gert", "Gustav", "Hans", "Harald", "Harri", "Harry", "Heinrich", "Heinz", "Hellmut", "Helmut", "Helmuth", "Herbert", "Hermann", "Holger", "Horst", "Hugo", "Ingo", "Jörg", "Jürgen", "Jacob", "Jakob", "Jan", "Jannik", "Jens", "Joachim", "Johann", "Johannes", "Jonas", "Jonathan", "Josef", "Joseph", "Julian", "Justin", "Kai", "Karl", "Karl-Heinz", "Karlheinz", "Karsten", "Kay", "Kevin", "Klaus", "Kristian", "Kurt", "Lars", "Lennard", "Lennart", "Leon", "Lothar", "Louis", "Luca", "Lucas", "Ludwig", "Luis", "Luka", "Lukas", "Lutz", "Maik", "Manfred", "Marc", "Marcel", "Marco", "Marcus", "Mario", "Mark", "Marko", "Markus", "Martin", "Marvin", "Mathias", "Matthias", "Max", "Maximilian", "Meik", "Michael", "Mike", "Moritz", "Nick", "Niclas", "Nico", "Niels", "Niklas", "Niko", "Nils", "Noah", "Norbert", "Olaf", "Ole", "Oliver", "Oscar", "Oskar", "Otto", "Patrick", "Paul", "Peter", "Philip", "Philipp", "Phillipp", "Rainer", "Ralf", "Ralph", "Reiner", "René", "Richard", "Robert", "Rolf", "Rudolf", "Rudolph", "Sascha", "Sebastian", "Siegfried", "Simon", "Stefan", "Steffen", "Stephan", "Sven", "Swen", "Thomas", "Thorsten", "Tim", "Timm", "Tobias", "Tom", "Torsten", "Ulrich", "Uwe", "Volker", "Walter", "Walther", "Werner", "Wilhelm", "Willi", "Willy", "Wolfgang", "Yannic", "Yannick", "Yannik" };

        private string[] surnameArray = { "Müller", "Schmidt", "Schneider", "Fischer", "Weber", "Meyer", "Wagner", "Becker", "Schulz", "Hoffmann", "Schäfer", "Bauer", "Koch", "Richter", "Klein", "Wolf", "Schröder", "Neumann", "Schwarz", "Braun", "Hofmann", "Zimmermann", "Schmitt", "Hartmann", "Krüger", "Schmid", "Werner", "Lange", "Schmitz", "Meier", "Krause", "Maier", "Lehmann", "Huber", "Mayer", "Herrmann", "Köhler", "Walter", "König", "Schulze", "Fuchs", "Kaiser", "Lang", "Weiß", "Peters", "Scholz", "Jung", "Möller", "Hahn", "Keller", "Vogel", "Schubert", "Roth", "Frank", "Friedrich", "Beck", "Günther", "Berger", "Winkler", "Lorenz", "Baumann", "Schuster", "Kraus", "Böhm", "Simon", "Franke", "Albrecht", "Winter", "Ludwig", "Martin", "Krämer", "Schumacher", "Vogt", "Jäger", "Stein", "Otto", "Groß", "Sommer", "Haas", "Graf", "Heinrich", "Seidel", "Schreiber", "Ziegler", "Brandt", "Kuhn", "Schulte", "Dietrich", "Kühn", "Engel", "Pohl", "Horn", "Sauer", "Arnold", "Thomas", "Bergmann", "Busch", "Pfeiffer", "Voigt", "Götz", "Seifert", "Lindner", "Ernst", "Hübner", "Kramer", "Franz", "Beyer", "Wolff", "Peter", "Jansen", "Kern", "Barth", "Wenzel", "Hermann", "Ott", "Paul", "Riedel", "Wilhelm", "Hansen", "Nagel", "Grimm", "Lenz", "Ritter", "Bock", "Langer", "Kaufmann", "Mohr", "Förster", "Zimmer", "Haase", "Lutz", "Kruse", "Jahn", "Schumann", "Fiedler", "Thiel", "Hoppe", "Kraft", "Michel", "Marx", "Fritz", "Arndt", "Eckert", "Schütz", "Walther", "Petersen", "Berg", "Schindler", "Kunz", "Reuter", "Sander", "Schilling", "Reinhardt", "Frey", "Ebert", "Böttcher", "Thiele", "Gruber", "Schramm", "Hein", "Bayer", "Fröhlich", "Voß", "Herzog", "Hesse", "Maurer", "Rudolph", "Nowak", "Geiger", "Beckmann", "Kunze", "Seitz", "Stephan", "Büttner", "Bender", "Gärtner", "Bachmann", "Behrens", "Scherer", "Adam", "Stahl", "Steiner", "Kurz", "Dietz", "Brunner", "Witt", "Moser", "Fink", "Ullrich", "Kirchner", "Löffler", "Heinz", "Schultz", "Ulrich", "Reichert", "Schwab", "Breuer", "Gerlach", "Brinkmann", "Göbel", "Blum", "Brand", "Naumann", "Stark", "Wirth", "Schenk", "Binder", "Körner", "Schlüter", "Rieger", "Urban", "Böhme", "Jakob", "Schröter", "Krebs", "Wegner", "Heller", "Kopp", "Link", "Wittmann", "Unger", "Reimann", "Ackermann", "Hirsch", "Schiller", "Döring", "May", "Bruns", "Wendt", "Wolter", "Menzel", "Pfeifer", "Sturm", "Buchholz", "Rose", "Meißner", "Janssen", "Bach", "Engelhardt", "Bischoff", "Bartsch", "John", "Kohl", "Kolb", "Münch", "Vetter", "Hildebrandt", "Renner", "Weiss", "Kiefer", "Rau", "Hinz", "Wilke", "Gebhardt", "Siebert", "Baier", "Köster", "Rohde", "Will", "Fricke", "Freitag", "Nickel", "Reich", "Funk", "Linke", "Keil", "Hennig", "Witte", "Stoll", "Schreiner", "Held", "Noack", "Brückner", "Decker", "Neubauer", "Westphal", "Heinze", "Baum", "Schön", "Wimmer", "Marquardt", "Stadler", "Schlegel", "Kremer", "Ahrens", "Hammer", "Röder", "Pieper", "Kirsch", "Fuhrmann", "Henning", "Krug", "Popp", "Conrad", "Karl", "Krieger", "Mann", "Wiedemann", "Lemke", "Erdmann" };

        public Process generateProcess()
        {
            Process proc = new Process();         
            proc.employee = generateEmployee();
            proc.task = generateTask();
            
            return proc;
        }

        private Employee generateEmployee()
        {
            Employee emp = new Employee();
            emp.gender = (Employee.Gender)rnd.Next(0,2);
            if(emp.gender == Employee.Gender.weiblich)
                emp.forename = female_forenameArray[rnd.Next(0, female_forenameArray.Length + 1)];
            else
                emp.forename = male_forenameArray[rnd.Next(0, male_forenameArray.Length + 1)];
            emp.surname = surnameArray[rnd.Next(0, surnameArray.Length + 1)];            

            return emp;
        }        

        private Task generateTask()
        {
            Task task = new Task();
            task.name = "TestTask" + (Properties.Settings.Default.TaskNr++);
            task.description = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.";
            task.startTime = DateTime.Now.AddDays(rnd.Next(-10, 11));
            task.finishTime = task.startTime.AddDays(rnd.Next(1, 11));

            return task;
        }
    }

}
