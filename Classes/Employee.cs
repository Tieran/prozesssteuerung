﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prozessteuerung
{
    public class Employee
    {
        public string surname { get; set; }
        public string forename { get; set; }
        public Gender gender { get; set; }

        public enum Gender
        {
            weiblich, männlich
        }
    }    
}
