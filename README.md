# README #

## Prozess Steuerung

### Idee
Erstellung einer Anwendung zur Darstellung von Prozessen. Jeder Prozess (Process) bestehen aus Aufgaben (Task) und zugewiesenen Mitarbeitern (Employee).

### Konzept
In einer ListBox werden die einzelnen Prozess Instanzen dargestellt.
Mit einem Doppelklick auf einen der ListBox Items öffnet sich das "Bearbeitungs Fenster" (EditWindow) an dass das ausgewählte Objekt übergeben wird.
Mit einem klick auf den Knopf "Neuer Prozess" (b_newProcess) wird das "Bearbeitungs Fenster" (EditWindow) mit einem leeren Prozess Objekt geöffnet.
Um einen Prozess aus der Liste zu löschen wählt man den zu löschenden Prozess aus und klickt auf "Prozess löschen" (b_delProcess).

#### DataBinding
Die erstellten Prozesse werden in einer statischen ObservableCollection _processList gespeichert, worauf auch der ItemSource der ListBox gesetzt.

#### Mockup
![Alt text](./Concept/Mockup.jpg)

#### Klassen
![Alt text](./Concept/Classes.jpg)
![Alt text](./Concept/Windows.jpg)